package ch.heig.pdg

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.*
import java.lang.Exception
import kotlin.concurrent.thread
import android.widget.Toast
import android.app.Activity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.*


private const val TAG = "SOO-UI "
private const val ENABLE_BT = 42

val uuid: UUID = UUID.fromString("42424242-feed-dead-beef-314159265359")

class MainActivity : AppCompatActivity() {

//    private var socket: BluetoothSocket? = null
    private var serialized : ByteArray = ByteArray(1000)

    // this is called as the result of the intent to turn BT on
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)

            if (requestCode == ENABLE_BT) {
                if (resultCode == Activity.RESULT_CANCELED) {
                    exitAppNicely("Bluetooth not turned on, exiting.")
                } else {
                    showUI()
                }
            }
        } catch (ex: Exception) {
            exitAppNicely("Unexpected error, sry: " + ex.message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            exitAppNicely("Device doesn't support Bluetooth, exiting.")
        }

        if (bluetoothAdapter?.isEnabled == false) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, ENABLE_BT)
        }
        else {
            showUI()
        }
    }

    // BroadcastReceiver triggered when a new BT device is found.
    private val btEventsReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    Log.i(TAG, "found device: "+ device.address)
                    addBtDeviceToList( device, context)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
        try { unregisterReceiver(btEventsReceiver) } catch (e: Exception){Log.i(TAG, "failed to unregister BT receiver: "+e.message)}
    }



    private fun addBtDeviceToList (device: BluetoothDevice,context: Context) {
        val line = TextView(context)
        val params =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(15, 25, 5, 5)
        line.text = if (device.name.isNullOrBlank().not()) device.name else device.address
        line.setOnClickListener {
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
            try { unregisterReceiver(btEventsReceiver) } catch (e: Exception){Log.i(TAG, "failed to unregister BT receiver: "+e.message)}

            handleMEconnection(device)
        }
        deviceList.addView(line, params)
    }

    private fun handleMEconnection (device: BluetoothDevice) {
//            Log.i(TAG,  "device uuids: " + device.uuids.fold("") { s, u -> s+"  "+u.uuid} )
        // no idea how to retrieve a not hardcoded uuid, position in uuids table might be different.
        val socket = device.createRfcommSocketToServiceRecord( uuid )//device.uuids[0].uuid)

        thread (start = true) {
            Log.i(TAG, "connecting to: " + socket.remoteDevice.name)
            socket.connect()

            val outputStream = socket.outputStream
            val inputStream = socket.inputStream

            try {

                while (socket.isConnected) {
                    inputStream.read(serialized)
                    val read = String(serialized)
//                        Log.i(TAG, "received: $read")

                    val ui = parseXMLtoView(read, socket)
                    // updating UI can be done only on UI thread
                    runOnUiThread {
                        mainContainer.removeAllViews()
                        mainContainer.addView(ui)
                    }
                }

            } catch(e: Exception) {
                Log.e(TAG, "error in communication thread", e)
            } finally {
                outputStream.close()
                inputStream.close()
                socket.close()
            }
        }
    }

    private fun parseXMLtoView (xml: String, socket: BluetoothSocket): LinearLayout {
        assert(xml.startsWith("<soo-view>"))

        val xpp = XmlPullParserFactory.newInstance().newPullParser()
        xpp.setInput(StringReader(xml))

        var eventType = xpp.eventType
        var currentTag: TextView? = null // Button is also a TextView
        val newDisplay = LinearLayout(baseContext) // base container to add elements
        newDisplay.orientation = LinearLayout.VERTICAL
        val params =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(15, 10, 5, 5)

        while (eventType != XmlPullParser.END_DOCUMENT) {
            when (eventType) {
                // found a starting tag
                XmlPullParser.START_TAG -> when (xpp.name) {
                    // recognizes these 3 types of tags:
                    "button" -> currentTag = Button(baseContext)
                    "text" -> currentTag = TextView(baseContext)
                    "action" -> if (currentTag is Button) { // could add TextViews to enable clicks on them
                        xpp.next()
                        val cmdToSend = xpp.text.toByteArray()
                        currentTag.setOnClickListener {
                            if (socket.isConnected) { // protecting against app closed on ME
//                                Log.i(TAG, "clicked button! sending cmd: ${String(cmdToSend)}")
                                socket.outputStream.write(cmdToSend)
                                socket.outputStream.flush()
                            }
                            else exitAppNicely("remote app closed, exiting.")
                        }
                    }
                }
                // text inside a tag
                XmlPullParser.TEXT -> if (xpp.text.isNotBlank()) {currentTag?.text = xpp.text}
                // end of tag, adding element to view if text or button
                XmlPullParser.END_TAG -> { if (xpp.name == "button" || xpp.name == "text") {
                    newDisplay.addView(currentTag, params)
                    currentTag = null
                }}
            }
            eventType = xpp.next()
        }
        return newDisplay
    }

    private fun showUI () {

        setContentView(R.layout.activity_main)
        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        // listing already known BT devices
        val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        //Log.d(TAG, pairedDevices.toString())
        pairedDevices?.forEach { device ->
            addBtDeviceToList(device, baseContext)
        }

        // starts discovery of new devices on button click
        findViewById<Button>(R.id.connectBtn).setOnClickListener {
            registerReceiver(btEventsReceiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
            bluetoothAdapter?.startDiscovery()
        }
    }

    private fun exitAppNicely (reason: String) {
        Toast.makeText(baseContext, reason, Toast.LENGTH_LONG).show()
        // delaying closing of app to show the message
        object : CountDownTimer(3000,3000) {
            override fun onTick(millisUntilFinished: Long) { /*Not needed*/ }

            override fun onFinish() {
                finishAndRemoveTask()
                System.exit(0)
            }
        }.start()
    }
}
