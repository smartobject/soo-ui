package ch.heig.pdg

import android.bluetooth.BluetoothSocket
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView

/**
 * Created on 2020-05-11.
 * @author Max
 */
class TestView : RemoteView {

    val getView = { baseContext: Context ->

        val newDisplay = LinearLayout(baseContext)
        newDisplay.orientation = LinearLayout.VERTICAL

        val param1 =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        //param1.bottomMargin = 100
        param1.setMargins(0, 150, 0, 300)
        //param1.gravity = Gravity.CENTER_HORIZONTAL

        val text = TextView(baseContext)
        text.text = "It is alive! This view was generated from code, serialized and deserialized using only its interface! =D"
        text.setTextColor(Color.BLACK)
        //text.layout(0,100,0,0)
        val button = Button(baseContext)
        button.text = "I'm useless but I'm here ;)"
        // button.gravity = Gravity.CENTER_VERTICAL

        newDisplay.addView(text, param1)
        newDisplay.addView(button)

    newDisplay
    }

    override val run = { viewContainer: ViewGroup, baseContext: Context, comm: BluetoothSocket ->

        viewContainer.removeAllViews()
        viewContainer.addView( getView(baseContext) )

        // TODO: send commands to other device, and loop to get updates


    }

}