package ch.heig.soo_me

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.util.Log
import java.io.IOException
import java.util.*

/**
 * Created on 2020-06-01.
 * @author Max
 */
val uuid: UUID = UUID.fromString("42424242-feed-dead-beef-314159265359")
const val TAG = "SOO_ME"

class BtSrvController : Thread() {
    private var cancelled: Boolean
    private val serverSocket: BluetoothServerSocket?

    init {
        val btAdapter = BluetoothAdapter.getDefaultAdapter()
        if (btAdapter != null) {
            this.serverSocket = btAdapter.listenUsingRfcommWithServiceRecord("soo-me", uuid)
            this.cancelled = false
        } else {
            this.serverSocket = null
            this.cancelled = true
        }

    }

    override fun run() {
        var socket: BluetoothSocket

        while ( !cancelled ) {
            try {
                Log.i(TAG, "accepting connection")
                socket = serverSocket!!.accept()
            } catch(e: IOException) {
                Log.i(TAG, "error accepting connection")
                break
            }

            if (!this.cancelled && socket != null) {
                Log.i(TAG, "Connected to: ${socket.remoteDevice.name}")
                // delegating handling to a new thread, should be able to handle multiples devices at the same time
                BtServer(socket).start()
            }
        }
    }

    fun cancel() {
        this.cancelled = true
        this.serverSocket!!.close()
    }
}