package ch.heig.soo_me

import android.bluetooth.BluetoothSocket
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created on 2020-06-01.
 * @author Max
 */

class BtServer (private val socket: BluetoothSocket): Thread() {
    private val inputStream = this.socket.inputStream
    private val outputStream = this.socket.outputStream
    private var timeShift = 0
    private var updateFreq = 1000L


    override fun run() {
        try {
            val bytes = ByteArray(100)

            // launching a new coroutine in background to send data periodically
            GlobalScope.launch {
                while (socket.isConnected) {
                    // sending the UI as XML
//                    Log.i(TAG, genXML(getTime()))
                    outputStream.write( genXML(getTime()).toByteArray())
                    outputStream.flush()
//                    Log.i(TAG, "sent UI!")
                    delay (updateFreq) // non-blocking delay for updateFreq second
                }
            }

            // reading commands from client, setting parameters accordingly and sending new UI
            while (socket.isConnected) {

//                Log.i(TAG, "waiting for command")
                inputStream.read(bytes)
//                Log.i(TAG, "Command received: ${String(bytes)}")
                execCmd( String(bytes) )
                outputStream.write( genXML(getTime()).toByteArray())
                outputStream.flush()
//                Log.i(TAG, "updated UI sent!")
            }

        } catch (e: Exception) {
            Log.e(TAG, "error in server", e)
        } finally {
            inputStream.close()
            outputStream.close()
            socket.close()
        }
    }

    private fun getTime () : String {
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.FRENCH)
        val time = Calendar.getInstance()
        time.add(Calendar.SECOND, timeShift)
        return dateFormat.format(time.time)
    }

    private fun genXML (time: String) :String {

        return  "<soo-view>" +
                "  <text> Time on Smart Object:\n $time </text>" +
                "  <button> add 1 hour " +
                "    <action>TS+3600</action>" +
                "  </button>" +
                "  <button> sub 1 hour " +
                "    <action>TS-3600</action>" +
                "  </button>" +
                "  <button> add 1 min " +
                "    <action>TS00060</action>" +
                "  </button>" +
                "  <button> sub 1 min " +
                "    <action>TS-0060</action>" +
                "  </button>" +
                "  <text> refresh every: </text>" +
                "  <button> 1 sec " +
                "    <action>REF1</action>" +
                "  </button>" +
                "  <button> 5 sec " +
                "    <action>REF5</action>" +
                "  </button>" +
                "</soo-view>"
    }

    private fun execCmd (cmd: String) {

        when {
            cmd.startsWith("TS") -> timeShift += cmd.substring(2, 7).toInt()
            cmd.startsWith("REF") -> updateFreq = cmd.substring(3,4).toLong() * 1000
        }
    }


}