package ch.heig.soo_me

import android.bluetooth.BluetoothAdapter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (BluetoothAdapter.getDefaultAdapter()?.isEnabled == false)
            // enabling Bt without prompt, smartObjects might have no screen.
            BluetoothAdapter.getDefaultAdapter().enable()
        while (BluetoothAdapter.getDefaultAdapter()?.isEnabled == false) {/* waiting for bt to activate*/}
        BtSrvController().start()
    }

}
