package ch.heig.soo_me

import android.bluetooth.BluetoothSocket
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import kotlin.concurrent.thread

/**
 * Created on 2020-05-11.
 * @author Max
 */
class TestView : RemoteView {

    val getView = { baseContext: Context, socket: BluetoothSocket ->

        val newDisplay = LinearLayout(baseContext)
        newDisplay.orientation = LinearLayout.VERTICAL

        val param1 =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        //param1.bottomMargin = 100
        param1.setMargins(0, 150, 0, 300)
        //param1.gravity = Gravity.CENTER_HORIZONTAL

        val text = TextView(baseContext)
        text.text = "It is alive! This view was generated from code, serialized in another app and deserialized using only its interface! =D"
        text.setTextColor(Color.BLACK)
        //text.layout(0,100,0,0)
        val button = Button(baseContext)
        button.text = "add 1 hour to time"
        // button.gravity = Gravity.CENTER_VERTICAL

        // sending message to increment time shift
        button.setOnClickListener {
            socket.outputStream.write("TS1".toByteArray())
            socket.outputStream.flush()
        }

        newDisplay.addView(text, param1)
        newDisplay.addView(button)

    newDisplay
    }

    override val run = { viewContainer: ViewGroup, baseContext: Context, comm: BluetoothSocket ->

        viewContainer.removeAllViews()
        viewContainer.addView( getView(baseContext, comm) )

        thread (start = true) {
            val text = TextView(baseContext)
            viewContainer.addView(text)
            while (true) {
                val read = ByteArray(100)
                comm.inputStream.read(read)
                viewContainer.removeView(text)
                text.text = read.toString()
                Log.i(TAG, "text received: " + text.text)
                viewContainer.addView(text)
            }
        }
    }

}