package ch.heig.soo_me

import android.bluetooth.BluetoothSocket
import android.content.Context
import android.view.View
import android.view.ViewGroup

/**
 * Created on 2020-05-11.
 * @author Max
 */
interface RemoteView : java.io.Serializable {

//    val getView: (baseContext: Context) -> View
    val run: (viewContainer: ViewGroup, baseContext: Context, comm: BluetoothSocket) -> Any

}
