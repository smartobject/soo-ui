# soo-ui

## SOO User Interface  

This project is devoted to the development of a generic UI application running on Android tablet (or smartphone). This app is designed to host a user interface sent by any Mobile Entity (ME) as XML, and is able to send back commands to the ME to adapt its behavior or settings.

### Workflow and Implementation
The UI application connects to the ME via bluetooth, and retrieves the UI descriptor encoded in XML format. This file can also contain the commands recognized by the ME that will be sent back when clicking on the corresponding buttons. For now the UI can be made only of TextViews and Buttons, but this can be extended to support any kind of android graphical elements. The commands can also be any String, their parsing must be implemented in the ME.

The ME application acts as the "server", by accepting bluetooth connections, starting threads to handle the connection with the UI app and responding to UI commands. This should support multiple simultaneous connections from UI apps, but couldn't be tested since I "only" got 2 phones. The handling thread launches a coroutine with a loop to send periodically new data to the UI app, and has another loop to listen for commands sent by the UI app. When a command is received, the resulting changes are sent back immediately to the UI app.

The UI application first displays all known bluetooth devices, and a button to seek other devices but it is still not working at this time (the 2 devices can still be paired via the bluetooth preferences of any device). Then, by clicking on the ME name, the UI app connects to it, receives the UI, parses it and displays the resulting view. The XML parser adds the elements in a vertical LinearLayout in the order of appearance in the XML, and adds a click listener to buttons having an \<action> tag, sending the content inside this tag to the ME as command.  

Each time the ME sends an update to the UI app, the whole XML descriptor is sent. This can seem overkill for light updates, like changing only one field, but the XML should never be huge since only a few elements fits on one screen, so the process stays fast, and this allows to easily send a totally (or partially) new UI when desired (e.g. on each update, when a condition is met or on a button click, for instance a menu can easily be done). 

### XML structure
The view XML's structure is as follows:  
\<soo-view>  
&emsp;&emsp;\<text> any text to display \</text>  
&emsp;&emsp;\<button> button's text  
&emsp;&emsp;&emsp;&emsp;\<action>CMD\_TO\_SEND\</action>  
&emsp;&emsp;\</button>  
\</soo-view>  
Texts and buttons can appear in any order inside the \<soo-view> tag.  



### Problems encountered with the implementation:

The first attempt was to directly send a class implementing an interface known on both sides from the ME, containing all graphical elements and treatment logic to be loaded by the UI app, but this has several flaws:  
Firstly, a class is hardly deserializable only knowing its interface, this could be accomplished as trials inside the same application via a ByteArray serialization / deserialization, but it turned out that this worked only because the concrete class was known by the app. Classes or pieces of code must be compiled (as .dex, possibly bundled inside a .jar or .apk) to be executed, it didn't work even when trying to send only a value containing a lambda.   
The 2 solutions left were to compile the code inside the UI app, but this would have meant to embed the JDK inside the app, this is not an easy problem and it would have made the app very heavy, adding a huge level of complexity and hence seemed overkilled.  
The second solution was to embed the compiled class (as .dex) in the ME app, since it shouldn't be modified in the process, to send it to te UI and load it dynamically via the DexClassLoader class (mainly used by the android OS), but this as raised a lot of problems, the first being that the process is scarcely documented, and all Stackoverflow questions asking about it were very old and not exactly related to our problem. The process is also advised against by Google themselves (it has obvious security flaws), and prohibited for apps distributed on the play store. The .dex could be sent to the UI app, but I couldn't go further because of a bad checksum error (even when the .dex was not modified and had the same name as the compilation output).  
The fallback was to describe the UI as XML and have commands for interacting with the ME as presented above. (I can't resist to state that this was roughly my first idea when reading this project's spec...)


### Possible improvements

As stated earlier, the whole UI is sent on each update from the ME, this could be changed to a partial update: it would need to add unique IDs to elements (can be done via View.generateViewId() ) to update instead of replacing them, but it quickly adds complexity to the update and communication processes (XML could not be so simple, need to keep a list of attributed ids matching XML ids).  

The new bluetooth device discovery inside the app is not finding any device while not producing errors, this can be worked around by pairing the 2 devices in the bluetooth preferences of any device, but it would be better if it could be done from inside the UI app.  

There is no button to disconnect from the ME, one could be added, for now the app must be closed and relaunched to connect to another ME.  

The way of reading the transferred data puts the whole content of the buffer into a string, this is no problem for the XML parser but one must be careful when recepting commands, substrings with known sizes should be used for parsing numbers, parsing string-only commands can be done using startsWith() method. This is also subject to improvements since it gave me headaches to find a simple solution to read only the textual content. (A parameter with the size could be sent beforehand to ajust the buffer size but again it complicates the communication.)  

Only text fields and buttons are supported by now, which already allows a lot of possibilities, but doubtlessly any android graphical component can be implemented.

### Pipeline

The continuous integration pipeline was created with the help of the jangrewe/gitlab-ci-android docker image, which embeds everything needed to compile an android app. See: [https://hub.docker.com/r/jangrewe/gitlab-ci-android/]()  
The build pipeline stayed very simple since I am a total stranger to Gradle tools, it just builds the main application and exits.

### How to run

This project was created using IntelliJ ultimate (free for educational purposes), just open each project in it (or in android studio) and run them.

